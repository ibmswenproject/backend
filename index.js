const express = require('express');
const bodyParser = require('body-parser');
const formidable = require('formidable'); 
const mongoose = require('mongoose');
const routes = require('./routes/api');

require('dotenv').config();

const app = express();

const port = process.env.PORT || 5000;

//connect to the database
mongoose.connect(process.env.DB, { useNewUrlParser: true,
  useUnifiedTopology: true})

  .then(() => console.log(`Database connected successfully`))
 
  .catch(err => console.log(err));

//since mongoose promise is depreciated, we overide it with node's promise
mongoose.Promise = global.Promise;

//send response to client
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//app.use(fileRoute);//watch for files

app.use(bodyParser.json());
// for parsing application/xwww-
app.use(bodyParser.urlencoded({ extended: true }));
//form-urlencoded

// for parsing multipart/form-data

app.use(express.static('files'));
app.use('/api', routes);

//log error
app.use((err, req, res, next) => {
  console.log(err);
  next();
});

//server listening
app.listen(port, () => {
  console.log(`Server running on port ${port}`)
});

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create schema for appreciation
const AppreciationtokenSchema = new Schema({
  sender: { type: String, required: [true] },
  badge: { type: String, required: [true] },
  sentence: { type: String, required: false, default: "Good work" },
  time: { type: String, required: true }

})

//create model for appreciation
const Appreciationtoken = mongoose.model('appreciationtoken', AppreciationtokenSchema);

module.exports = Appreciationtoken;

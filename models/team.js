const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create schema for team
const TeamSchema = new Schema({
    
  name: { type: String, required: [true, 'The todo text field is required'] },
  members: { type: Array, required: true},
  id: { type: Number, required: true},
  app: { type: Array, required: false},
  status: { type: Boolean, required: true},
  admin: {type: String, required: true}

})

//create model for team
const Team = mongoose.model('team', TeamSchema);

module.exports = Team;

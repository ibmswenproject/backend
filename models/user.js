const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//create schema for user
const UserSchema = new Schema({
  isAdmin: {type:Boolean},
  isLoggedIn: {type:Boolean},
  credentials: {
    name: { type: String, required: [true, 'The name text field is required'] },
    title: { type: String, required: [true] },
    pronouns: { type: String, required: true},
    email: { type: String, required: true },
    phone: {type: Number, required: true},
    slack: {type: String, required: true, default: 'https://www.slack.com' },
  },
  profile: {
    photo: {type: String, required: true},
    about: {type: String, required: true},
    teams: {type: Array, required: true}
    
  },
  app: {type: Array, required: true }

})

//create model for user
const User = mongoose.model('user', UserSchema);

module.exports = User;

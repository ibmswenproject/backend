const express = require('express');
const router = express.Router();
let User = require('../models/user');
let Team = require('../models/team');
require('dotenv').config();
const path = require('path');
const fs = require('fs'); 
const formidable = require('formidable'); 

const nodemailer = require('nodemailer')



// const transport = {
//   //all of the configuration for making a site send an email.
//   service: process.env.EMAIL_SERVICE,
//   server: process.env.EMAIL_SERVER,
//   port: process.env.EMAIL_PORT,
  
//   auth: {
//     user: process.env.EMAIL_USERNAME,
//     pass: process.env.EMAIL_PASSWORD,
//   },
// }
  
  


// const transporter = nodemailer.createTransport(transport);
//   transporter.verify((error, success) => {
//     if(error) {
//       //if error happened code ends here
//       console.error(error)
//     } else {
//       //this means success
//       console.log('ready to mail')
//     }
//   });

  


//find user by email
router.get('/users/:email', (req, res) => {
  User.find({ "credentials.email": req.params.email })
  
    .then((user) => {
      res.json(user)
    })
    .catch(err => res.status(400).json('Error: ' + err));

    
});

//find team by id
router.get('/teams/:id', (req, res) => {
  Team.find({ "id": req.params.id })
    .then((team) => {
      res.json(team)
    })
    .catch(err => res.status(400).json('Error: ' + err));
});

//get all users in database (currently used for testing)
router.get('/', (req, res) => {
  User.find()
    .then(users => res.json(users))
    .catch(err => res.status(400).json('Error: ' + err));
});

//add a user
router.post('/users', (req, res) => {

  const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){ 

       console.log(fields)
      console.log(files)

      var oldPath = files.photo.path;  
        var newPath = path.join(__dirname, '../files') 
                + '/'+files.photo.name 
                console.log(newPath)
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) console.log(err) 
            // return res.send("Successfully uploaded") 
        });

      const name2 = fields.firstname;
      const title = fields.title;
      const pronouns = fields.pronouns;
      const location = fields.location;
      const phone = fields.phone;
      const about = fields.about;
      const email = fields.email;
      
      // const slack = fields.slack;
      
      const newUser = new User({


        IsAdmin: false,
        IsLoggedIn: false,
    
        credentials: {
          name: name2,
          title: title,
          pronouns: pronouns,
          email: email,
          phone: phone,
          slack: "slack"
    
        },
        profile: {
          photo: files.photo.name,
          about: about,
          teams: [3, 6]
    
        },
        app: [Object]
    
      })
    
      newUser.save()
        .then(() => res.json('User added!'))
        .catch(err => res.status(421).json('Error: ' + err));
    
  })
}),
 
  

router.post('/edituser',  (req, res) => {

  const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){ 
    

      const name2 = fields.firstname;
      const title = fields.title;
      const pronouns = fields.pronouns;
      const email = fields.email;
      console.log(email);
    // const availibily = req.body.availability;
      const location = fields.location;
      var photo = "";
      if(files>0){
        photo = files.photo.name;
      var oldPath = files.photo.path; 
        var newPath = path.join(__dirname, '../files') 
                + '/'+files.photo.name 
                console.log(newPath)
        var rawData = fs.readFileSync(oldPath) 
      
        fs.writeFile(newPath, rawData, function(err){ 
            if(err) console.log(err) 
            // return res.send("Successfully uploaded") 
        });
      }
      else{
        photo = fields.photo
      }
     

  const phone = fields.phone;
  //const slack = req.body.slack;
  //const photo = req.file.photo;
  const about = fields.about;
  console.log(about);

  User.findOneAndUpdate({"credentials.email": email}, {
    credentials : {
    
    name : name2,
    title : title,
    pronouns : pronouns,
    email : email,
  },
  profile: {
    
      photo : photo,
      about : about

    }
    

  }, { new: true }, (err, doc) => {
    if (err) {
      console.log("Something wrong when updating data!");
    }
    console.log(doc);
  });
  });
  });
 

  

  //find user by id and delete
  router.delete('/users/:id', (req, res, next) => {
    User.findOneAndDelete({ "_id": req.params.id })
      .then(data => res.json(data))
      .catch(next)
  });

  //find user by email and add appreciation to app array
  router.post('/appr', (req, res) => {
    const senderdata = req.body.sender;
    const sender = senderdata.data;
    const receiver = req.body.receiver.data
    console.log(sender);
    const badge = req.body.badge.data;
    console.log(req.body.receiver);
    const comment = req.body.comment;

    let current = new Date();
    let cDate = current.getDate() + '-' + (current.getMonth() + 1) + '-' + current.getFullYear();
    let cTime = current.getHours() + ":" + current.getMinutes() + ":" + current.getSeconds();
    let dateTime = cDate + ' ' + cTime;

    const appreciationtoken = new Object({
      sender: sender,
      sentence: comment,
      badge: badge,
      time: dateTime,
    })

    var change = { $push: { app: appreciationtoken } }
    User.findOneAndUpdate({ "credentials.email": receiver }, change, { new: true }, (err, doc) => {
      if (err) {
        console.log("Something wrong when updating data!");
      }
      console.log(doc);
    });

    //make mailable object
    const mail = {
      from: process.env.THE_EMAIL,
      to: {receiver},
      subject: "You got appreciation!",
      text: `
      from: ${sender}
      

      contact: ${sender}

      message: 
      ${"Good job! Someone appreciates you!"}`
      
    }

     
    // transporter.sendMail(mail, (err,data) => {
    //   if(err) {
    //     res.json({
    //       status: 'fail'
    //     })
    //   } else {
    //     res.json({
    //       status: 'success'
    //     })
    //   }
    // })
  }),
  
  /**
   * create team
   */
  router.post('/teams', (req, res) => {
    const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){
    const name = fields.name;
    const people = fields.people;
    const admin = field.admin;

    const newTeam = new Team({
      name: name,
      members: people,
      appreciation: [],
      admin: admin

    });

    newTeam.save()
      .then(() => res.json('Team added!'))
      .catch(err => res.status(421).json('Error: ' + err));
  });
  }),

/**
 * Add user to team. Find team by id and update, find user by email and add team id to teams array.
 */
router.post('/userteam', (req, res) => {
  const form = new formidable.IncomingForm(); 
    form.parse(req, function(err, fields, files){ 

      const team = fields.team;
      const users = fields.users;
      const name = fields.teamname;

  Team.findOneAndUpdate({ "id": team }, { $push: { people: user }, name: name }, { new: true }, (err, doc) => {
    if (err) {
      console.log("Something wrong when updating data!");
    }
    else {
      res.json('User added to team!');
    }

    console.log(doc);

  });

  User.findOneAndUpdate({ "credentials.email": user }, { $push: { profile: {teams: team} } }, { new: true }, (err, doc) => {
    if (err) {
      console.log("Something wrong when updating data!");
    }
    else {
      res.json('User added to team!');
    }

    console.log(doc);

  });
});
 
}),

function (err, numberAffected, rawResponse) {
  //handle it
}

module.exports = router;
